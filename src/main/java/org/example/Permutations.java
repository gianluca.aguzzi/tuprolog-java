package org.example;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Theory;
import alice.tuprolog.exceptions.MalformedGoalException;
import alice.tuprolog.exceptions.NoMoreSolutionException;
import alice.tuprolog.exceptions.NoSolutionException;

import java.io.IOException;
import java.io.InputStream;

public class Permutations {
    final static String knowledge =
            "member([H|T],H,T).\n" +
            "member([H|T],E,[H|T2]):- member(T,E,T2).\n" +
            "permutation([],[]).\n" +
            "permutation(L,[H|TP]) :- member(L,H,T),permutation(T,TP).";
    public static void main(String[] args) throws MalformedGoalException, NoSolutionException, NoMoreSolutionException, IOException {
        final InputStream prologDefinitions = ClassLoader.getSystemResourceAsStream("permutations.pl");
        final Prolog prolog = new Prolog();
        final Theory knowledgeBase = Theory.parseWithStandardOperators(knowledge);
        System.out.println("Knowledge:");
        System.out.println(knowledgeBase);
        prolog.setTheory(knowledgeBase);
        final String query = "permutation([1,2,3],X).";
        SolveInfo currentSolution = prolog.solve(query);
        System.out.println("Solutions: ");
        while(currentSolution.hasOpenAlternatives()) {
            System.out.println(currentSolution.getSolution());
            currentSolution = prolog.solveNext();
        }
    }

}