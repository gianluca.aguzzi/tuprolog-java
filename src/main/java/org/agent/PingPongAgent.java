package org.agent;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.Var;

public class PingPongAgent extends Agent {
    private Term state = Term.createTerm("ping");
    @Override
    public Theory baseKnowledge() {
        return Theory.parseWithStandardOperators("ping(pong). \n" + "pong(ping).");
    }

    @Override
    public Term sense() {
        return state;
    }

    @Override
    public Term goal() {
        String nextAction = state.toString();
        return Term.createTerm(nextAction + "(X).");
    }

    @Override
    public void act(Term action) {
        if(action instanceof Struct && ((Struct) action).getArg(0) instanceof Var) {
            var nextAction = ((Struct) action).getArg(0).getTerm();;
            state = nextAction;
        }
    }
}
