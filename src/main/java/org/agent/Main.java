package org.agent;

import alice.tuprolog.exceptions.NoSolutionException;

public class Main {
    public static void main(String[] args) throws NoSolutionException {
        final var agent = new PingPongAgent();
        final int iterations = 10;
        for (int i = 0; i < iterations; i++) {
            System.out.println("Ready to: " + agent.sense() + " itself");
            agent.reasoningCycle();
        }
    }
}
