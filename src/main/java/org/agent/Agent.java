package org.agent;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.exceptions.NoSolutionException;

public abstract class Agent {
    public abstract Theory baseKnowledge();
    public abstract Term sense();

    public abstract Term goal();
    public abstract void act(Term action);

    public final void reasoningCycle() throws NoSolutionException {
        final Term perception = sense();
        final Prolog prolog = new Prolog();
        prolog.addTheory(baseKnowledge());
        prolog.addTheory(Theory.of(perception));
        final SolveInfo solution = prolog.solve(goal());
        if(solution.isSuccess()) {
            act(solution.getSolution());
        }
    }
}
